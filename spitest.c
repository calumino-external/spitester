/*
* SPI testing utility (using spidev driver)
*
* Copyright (c) 2021  Calumino
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License.
*
* Compile with "make spitest.c"
*/

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>
#include <curses.h>         /* curses.h includes stdio.h */
#include "utils.h"

static void pabort(const char *s)
{
    perror(s);
    abort();
}

static float ctsGen       = 1.1; //CHANGE ACCORDINGLY, OR PASS IN WITH "-g 1.1". This is the default if nothing passed in.
int reset                 = 0;
// default values (Raspberry PI specific)

static const char *device = "/dev/spidev0.0";
static uint8_t mode       = 3;
static uint8_t bits       = 8;
static uint32_t speed     = 976000;
static int frameReadyPin1_0 = 6; //GPIO 25 -> wiringPi pin 6 (1.0)
static int frameReadyPin1_1 = 27; //GPIO 16 -> (1.1) wiringPi pin 27 (on RPi HAT Board - CRPH16-V1.0A)
static int frameReadyPin2_0 = 6; //GPIO 25 -> (2.0) wiringPi pin 6 (on RPi adapter board - CRPA21-V1.1A)
static uint8_t *buffer    = NULL;
int bufflen        = 0;
static int bufflen1_0     = 2434; //2434 (1 dummy byte) for 1.0
static int bufflen1_1     = 2438; //2438 (3 additional dummy bytes) for 1.1 - this actually makes no difference
static int bufflen2_0     = 2438; //2438 (3 additional dummy bytes) for 1.1 - this actually makes no difference
static int fd;
static uint8_t *rx        = NULL;
static int nrstPin_1_1    = 24; //GPIO19 -> wiringPi pin 24 (1.1 only) (on RPi HAT Board - CRPH16-V1.0A)
static int nrstPin_2_0    = 2; //GPIO 27 -> wiringPi pin 2 (2.0 only) (on RPi adapter board - CRPA21-V1.1A)

// caller must check that tx != NULL
static void transfer(uint8_t* txdata, int txlen, int fd)
{
    int ret;

    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long)txdata,
        .rx_buf = (unsigned long)rx,
        .len = txlen, 
        .delay_usecs = 0,
        .speed_hz = speed,
        .bits_per_word = bits,
    };

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    if (ret < 1)
        pabort("can't send spi message");
}

uint16_t *convert_data(uint8_t *buffer, int bufflen)
{
    int i = 0;
    int final_data_len = bufflen/2;

    uint8_t tmp;
    uint8_t *data = NULL;
    uint16_t *final_data = NULL;
    
    data = (uint8_t *) calloc(bufflen, sizeof(uint8_t));
    final_data = (uint16_t *) calloc(final_data_len, sizeof(uint16_t));
    
    if (NULL != data && NULL != final_data) {

        // reverse bit
        for (i = 0; i < bufflen; i++) {
            tmp = reverse_byte(buffer[i]);
            data[i] = tmp;
        }

        // create 2 byte vals
        for (i = 0; i < final_data_len; i++) {
            uint8_t lsb = 0;
            uint8_t msb = 0;

            lsb = data[2*i];
            msb = data[2*i+1];
            final_data[i] = (msb << 8) + (lsb);
        }
    }
    else {
        pabort("out of memory");
    }

    free(data);

    return final_data;
}

static void parse_metadata(uint16_t *data)
{
    // int rowlen = 25;
    // int fields = 10;
    // char **metadata = NULL;

    // NOTE: data[0] contains the spi cmd response, that's why first
    // metadata info starts at 647/648 instead of 646
    int startField;
    if (ctsGen < 1.05){
        startField = 647;
    }
    else{
        //CTS Gen 1.1 has 2 extra dummy bytes (1 extra word), so start is 1 byte further
        startField = 648;
    }
    uint32_t buffer = 0;
    uint8_t itgr = 0;
    uint8_t frct = 0;

    //serial number
    buffer = (data[startField+1] << 16) | data[startField];
    mvprintw(6, 0, "serial_number %04d\n", buffer);

    // hw version
    buffer = (int)(data[startField + 3] << 16 | data[startField + 2]);
    mvprintw(7, 0, "hw_version %X\n", buffer);

    // software version
    buffer = (int)(data[startField+65] << 16 | data[startField + 64]);
    mvprintw(8, 0, "sw_version %X\n", buffer);

    // FOV
    buffer = (((data[startField+4] % 512) % 64) % 8);
    switch(buffer){
        case 0:
            mvprintw(9, 0, "FOV 30");
            break;
        case 1:
            mvprintw(9, 0, "FOV 45");
            break;
        case 2:
            mvprintw(9, 0, "FOV 60");
            break;
        case 3:
            mvprintw(9, 0, "FOV 100");
            break;
        case 4:
            mvprintw(9, 0, "FOV 120");
            break;
        case 5:
            mvprintw(9, 0, "FOV 150");
            break;
        default:
            mvprintw(9, 0, "NO FOV");
    }

    //Lens centre coordinates
    float lensX = ((data[startField+6] >> 8))/8.0;
    float lensY = ((data[startField+6]) % 256)/8.0;
    mvprintw(10, 0, "Lens centre X %2.3f", lensX);
    mvprintw(11, 0, "Lens centre Y %2.3f", lensY);

    //ROI
    int colStart = ((data[startField+8] % 256) % 8);
    int rowStart = ((data[startField+8] % 256) / 8.0);
    int validCols = ((data[startField+9]) >> 11) + 23;
    int validRows = ((data[startField+8]) / 256.0) + 14;
    if (validRows == 269) //Remainder returned 255 instead of 0 (+14 = 269)
        validRows = 14;
    mvprintw(12, 0, "Column start %2d", colStart);
    mvprintw(13, 0, "Row start %2d", rowStart);
    mvprintw(14, 0, "Number of columns %2d", validCols);
    mvprintw(15, 0, "Number of rows %2d", validRows);

    // FPGA temperature
    itgr = (uint8_t)(data[startField+10] >> 8);
    frct = (uint8_t)(data[startField+10] & 0xff);
    mvprintw(16, 0, "FPGA temp %d.%d", (int)itgr, (int)frct);

    // image sensor temperature
    itgr = (uint8_t)(data[startField+11] >> 8);
    frct = (uint8_t)(data[startField+11] & 0xff);
    if (ctsGen < 1.05){
        mvprintw(17, 0, "CMOS_T %d.%d", (int)itgr, (int)frct);
    }
    else {
        mvprintw(17, 0, "CMOS_T (analog) %d.%d", (int)itgr, (int)frct);
    }

    // Led temperature
    itgr = (uint8_t)(data[startField+12] >> 8);
    frct = (uint8_t)(data[startField+12] & 0xff);
    mvprintw(18, 0, "LED_T %d.%d\n", (int)itgr, (int)frct);

    // FT602
    itgr = (uint8_t)(data[startField+13] >> 8);
    frct = (uint8_t)(data[startField+13] & 0xff);
	if (ctsGen < 1.05){
        mvprintw(19, 0, "FT602_T %d.%d\n", (int)itgr, (int)frct);
	}
	else {
        mvprintw(19, 0, "DC_DC_T %d.%d\n", (int)itgr, (int)frct);
    }

    // flex1
    itgr = (uint8_t)(data[startField+14] >> 8);
    frct = (uint8_t)(data[startField+14] & 0xff);
    mvprintw(20, 0, "Flex1_T %d.%d\n", (int)itgr, (int)frct);

    // flex2
    itgr = (uint8_t)(data[startField+15] >> 8);
    frct = (uint8_t)(data[startField+15] & 0xff);
	if (ctsGen < 1.05){
        mvprintw(21, 0, "Flex2_T %d.%d\n", (int)itgr, (int)frct);
    }
    else {
        mvprintw(21, 0, "CMOS_T (digital) %d.%d\n", (int)itgr, (int)frct);
    }

    float accel_x = (int16_t)(data[startField+60] << 4)/1296.0/16.0; //16mg per bit, 12 bit signed aligned on 16 bit fields
    float accel_y = (int16_t)(data[startField+61] << 4)/1296.0/16.0;
    float accel_z = (int16_t)(data[startField+62] << 4)/1296.0/16.0;
    mvprintw(22, 0, "accel_x %5.5f\n", accel_x);
    mvprintw(23, 0, "accel_y %5.5f\n", accel_y);
    mvprintw(24, 0, "accel_z %5.5f\n", accel_z);

    // uptime
    buffer = (data[startField+57] << 16) | data[startField+56];
    mvprintw(25, 0, "uptime %d\n", buffer);

    // frames
    buffer = (data[startField+59] << 16) | data[startField+58];
    mvprintw(26, 0, "frame_count %d\n", buffer);
    
    refresh();
}

// used as callback for GPIO interrupts
static void do_transfer(void)
{
    int i = 0;

    uint16_t *data = NULL;
    uint8_t *readY16cmd = NULL;

    readY16cmd = (uint8_t*) calloc(1, sizeof(uint8_t));
    readY16cmd[0] = (uint8_t)0xF5;
    
    transfer(readY16cmd, bufflen, fd);

    data = convert_data(rx, bufflen);

    parse_metadata(data);

    // fflush(stdout);
    // print what we got
    // for (i = 647; i < bufflen/2; i++) {
    //  if (!(i % 38))
    //      puts("");
    //  fprintf(stdout, "%.4X ", data[i]);
    // }
    // puts("");

    if (NULL != data) 
        free(data);

    if (NULL != readY16cmd)
        free(readY16cmd);
}

static void print_usage(const char *prog)
{
    fprintf(stdout, "Usage: %s [-DsbLClpg]\n", prog);
    puts("  -d --device      device to use (default: '/dev/spidev0.0')\n"
         "  -s --speed       max speed in Hz\n"
         "  -g --gpio        gpio used as frame ready pin\n"
         "  -r --reset       reset CTS (1.1 only) (1/0)\n");
    exit(1);
}

static void parse_opts(int argc, char *argv[])
{
    while (1) {
        static const struct option lopts[] = {
            { "device",   required_argument, 0, 'd' },
            { "speed",    required_argument, 0, 's' },
            { "generation", required_argument, 0, 'g' },
            { "reset",    required_argument, 0, 'r' },
            { NULL, 0, 0, 0 },
        };

        int c = getopt_long(argc, argv, "d:s:g:r:", lopts, NULL);

        if (c == -1)
            break;

        switch (c) {
        case 'd':
            device = optarg;
            break;

        case 's':
            speed = atoi(optarg);
            break;

        case 'g':
            ctsGen = atof(optarg);
            break;

        case 'r':
            reset = atoi(optarg);
            break;

        default:
            print_usage(argv[0]);
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    int ret = 0;
    uint8_t* databuffer = NULL;

    /* init wiringPi */
    if (wiringPiSetup() < 0) {
        pabort("Unable to setup wiringPi");
    }

    /* parse arguments */
    parse_opts(argc, argv);

    fd = open(device, O_RDWR);
    if (fd < 0)
        pabort("can't open device");

    // spi mode
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
        pabort("can't set spi mode");

    ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
    if (ret == -1)
        pabort("can't get spi mode");

    // bits per word
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1)
        pabort("can't set bits per word");

    ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1)
        pabort("can't get bits per word");

    // max speed hz
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't set max speed hz");

    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't get max speed hz");

    else {
        /* init buffers */
        if (ctsGen < 1.05) {
            rx = (uint8_t *) calloc(bufflen1_0, sizeof(uint8_t));
            databuffer = (uint8_t *) calloc(bufflen1_0, sizeof(uint8_t));
            bufflen = bufflen1_0;
        }
        else if (ctsGen > 1.05 && ctsGen < 1.9){
            rx = (uint8_t *) calloc(bufflen1_1, sizeof(uint8_t));
            databuffer = (uint8_t *) calloc(bufflen1_1, sizeof(uint8_t));
            bufflen = bufflen1_1;
        }
		else if (ctsGen > 1.9) {
			rx = (uint8_t *) calloc(bufflen2_0, sizeof(uint8_t));
			databuffer = (uint8_t *) calloc(bufflen2_0, sizeof(uint8_t));
			bufflen = bufflen2_0;
		}
        /* init ncurses */
        initscr();
        scrollok(stdscr, 1);
        setscrreg(5, LINES-1);

        /* hide cursor */
        curs_set(0);

        if (reset == 1) {
			if (ctsGen < 1.05) {
                mvprintw(0, 0, "CTS RESET PIN DOES NOT EXIST ON Gen1.0. No reset performed!\n");
                delay(2000);
			}
			else if (ctsGen > 1.05 && ctsGen < 1.9) {
                pinMode(nrstPin_1_1, OUTPUT);
				digitalWrite(nrstPin_1_1, LOW);
				delay(1000);
				digitalWrite(nrstPin_1_1, HIGH);
				mvprintw(0, 0, "CTS RESET USING PIN %d (wiringPi notation)\n", nrstPin_1_1);
				delay(1000);
			}
			else if (ctsGen > 1.9) {
                pinMode(nrstPin_2_0, OUTPUT);
				digitalWrite(nrstPin_2_0, LOW);
				delay(1000);
				digitalWrite(nrstPin_2_0, HIGH);
				mvprintw(0, 0, "CTS RESET USING PIN %d (wiringPi notation)\n", nrstPin_2_0);
				delay(1000);
			}
        }

        mvprintw(0, 0, "device: %s\n", device);
        mvprintw(1, 0, "spi mode: %d\n", mode);
        mvprintw(2, 0, "bits per word: %d\n", bits);
        mvprintw(3, 0, "max speed: %d Hz (%d KHz)\n", speed, speed/1000);
		mvprintw(4, 0, "ctsGen chosen: %1.1f \n", ctsGen);
        if (ctsGen < 1.05){
            printw("frame ready GPIO: %d (wiringPi notation)\n", frameReadyPin1_0);
        }
        else if (ctsGen > 1.05 && ctsGen < 1.9){
            printw("frame ready GPIO: %d (wiringPi notation)\n", frameReadyPin1_1);
        }
		else if (ctsGen > 1.9){
			printw("frame ready GPIO: %d (wiringPi notation)\n", frameReadyPin2_0);
		}
        printw("\n");

        refresh();

        /* assign callback */    
        /*
        if (ctsGen < 1.05){
            if (wiringPiISR(frameReadyPin1_0, INT_EDGE_RISING, &do_transfer) < 0) {
                pabort("Unable to setup gpio interrupt");
            }
        }
        else {
            if (wiringPiISR(frameReadyPin1_1, INT_EDGE_FALLING, &do_transfer) < 0) {
                pabort("Unable to setup gpio interrupt");
            }
        }*/

        refresh();
        
        while (1) {
            delay(10);  // wait 10ms
            if (ctsGen < 1.05 && digitalRead(frameReadyPin1_0)){
                do_transfer();
            }
            else if (ctsGen > 1.05 && ctsGen < 1.9 && !digitalRead(frameReadyPin1_1)){
                do_transfer();
            }
			else if (ctsGen > 1.9 && !digitalRead(frameReadyPin2_0)){
				do_transfer();
			}
        }

        free(databuffer);
        free(rx);

        endwin();
    }
    
    close(fd);
    
    return EXIT_SUCCESS;
}
