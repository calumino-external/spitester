/**
 * author:  lorenzo chianura
 * mail:    lorenzo@calumino.com
 * company: Calumino
 * date:    09/06/2020
 * version: 0.1
 */
#pragma once
#include <stdint.h>

/* SPI commands */
#define READ_FRAME 0xAF  // Retrieve a Y16 IR frame from the CTS (with its metadata)

/* CTS INTERNAL STATUS FLAGS */
#define IDLE  0x00  // the interface is ready to accept a new request
#define BUSY  0x01  // the CPU is currently processing the last request
#define ERROR 0X02  // ERROR there was an error while processing the last request.

uint8_t reverse_byte(uint8_t x);
