# spitester

Little C cli application used to test CTS via SPI on a Raspberry PI.

## Getting started 
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
Please remember that the application is still under development and thus contains lots of bugs and unknown issues.

User must pass in the ctsGen accordingly with "-g 1.1", depending on the CTS type being used (1.0, 1.1, or 2.0). The default is 1.1 and this can be changed by changing the variable name and remaking.

User can also change the default frame ready and nrst pins by changing the frameReadyPinX_X and nrstPin_X_X variables, respectively. Note also that as Gen1.1 and Gen2.0 use the same SPI transaction type, the ctsGen is interchangable for them (i.e. ctsGen = 1.1 will work for Gen2.0 hardware and vice versa).

## Prerequisites
Execute the following command to install *wiringpi* and *ncurses* dependencies:

```
sudo apt install libncurses5-dev libncursesw5-dev wiringpi
```

## Compile and run
Use the `make` command to compile sources.
You will end up with a single executable binary called `spitester`.
Use:

```
./spitester --help 
```

to get all the available options.

Otherwise just use:

```
./spitester -g 1.1
```

or to toggle the reset pin:

```
./spitester -g 1.1 -r 1
```

to read metadata continuously.

If unit is Gen1.1 or Gen2.0, the NRST pin can be toggled to perform a soft reset (equivalent to a power cycle) using:

```
./spitester -r 1
```
Note that the nrstPin variable must be changed accordingly (if different to default).
